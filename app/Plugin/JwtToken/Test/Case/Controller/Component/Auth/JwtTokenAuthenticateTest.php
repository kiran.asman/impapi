<?php
/**
 * JwtAuthenticateTest file
 *
 * PHP 5
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2015 Kiran Kumar M <kiran@fingersfun.com>
 * @link          http://fingersfun.com CakePHP 2 Jwt Authentication
 * @package       JwtAuth.Test.Case.Controller.Component.Auth
 * @since         JwtAuth v 1.0
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AuthComponent', 'Controller/Component');
App::uses('JwtTokenAuthenticate', 'JwtAuth.Controller/Component/Auth');
App::uses('AppModel', 'Model');
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('Controller', 'Controller');


// A Mock Controller
class JwtControllerTest extends Controller
{
    public $name = "JwtControllerTest";
}
/**
 * Test case for JwtAuthentication
 *
 * @package       JwtAuth.Test.Case.Controller.Component.Auth
 */
class JwtTokenAuthenticateTest extends CakeTestCase
{
    public $fixtures = array('plugin.JwtAuth.user');

    /**
     * @var JwtTokenAuthenticate
     */
    public $auth;

    /**
     * @var Controller
     */
    public $Controller;

    /**
     * @var JWT
     */
    public $Jwt;

    /**
     * @var CakeResponse
     */
    public $response;
    /**
     * setup
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $Collection = new ComponentCollection();
        $this->auth = new JwtTokenAuthenticate($Collection, array(
            'fields' => array(
                'username' => 'name',
                'token' => 'auth_token',
            ),
            'parameter' => 'auth_token',
            'header' => 'X_JSON_WEB_TOKEN_AUTH',
            'userModel' => 'User',
            'scope' => array(),
            'recursive' => 0,
            'contain' => null,
            'pepper' => '9246560911'
        ));
        $password = Security::hash('9246560911', null, true);
        $User = ClassRegistry::init('User');
        $User->updateAll(array('password' => $User->getDataSource()->value($password)));
        $this->response = $this->getMock('CakeResponse');
        $this->Jwt = new JWT();
    }

    /**
     * test authenticate token as query parameter
     *
     * @return void
     */
    public function testAuthenticateTokenParameter() {
        $this->auth->settings['_parameter'] = 'token';

        $jwt = $this->_setToken('54321');
        $request = new CakeRequest('posts/index?auth_token=' . $jwt);

        $result = $this->auth->getUser($request, $this->response);

        $this->assertFalse($result);

        $expected = array(
            'id' => '1',
            'name' => 'kiran',
            'password' => '4c42325fca2ce6e54894b792f900fa3203601537',
            'token' => '54321',
            'email' => 'kiran@example.com',
            'created' => '2017-10-07 14:05:00',
            'updated' => '2017-10-07 14:05:00'
        );
        $jwt = $this->_setToken('54321');
        $request = new CakeRequest('posts/index?auth_token=' . $jwt);
        $result = $this->auth->getUser($request, $this->response);
        $this->assertEquals($expected, $result);
    }

    /**
     * test authenticate token as request header
     *
     * @return void
     */
    public function testAuthenticateTokenHeader() {
        $_SERVER['X_JSON_WEB_TOKEN_AUTH'] = $this->_setToken('54321');
        $request = new CakeRequest('posts/index', false);

        $result = $this->auth->getUser($request, $this->response);
        $this->assertFalse($result);

        $expected = array(
            'id' => '1',
            'name' => 'kiran',
            'password' => '4c42325fca2ce6e54894b792f900fa3203601537',
            'token' => '54321',
            'email' => 'kiran@example.com',
            'created' => '2017-10-07 14:05:00',
            'updated' => '2017-10-07 14:05:00'
        );
        $_SERVER['HTTP_X_JSON_WEB_TOKEN'] = $this->_setToken('54321');
        $result = $this->auth->getUser($request, $this->response);
        $this->assertEquals($expected, $result);
    }

    /**
     * @param string $userToken
     * @return string
     */
    private function _setToken($userToken)
    {
        $key = $this->auth->settings['pepper'];
        $token = array(
            "iss" => "http://example.org",
            "aud" => "http://example.com",
            "iat" => 1356999524,
            "nbf" => 1357000000,
            "user" => array(
                'name' => 'kiran',
                'token' => $userToken
            )
        );

        return $this->Jwt->encode($token, $key);
    }
}
