<?php
/**
 *
 * PHP 5
 *
 * @company 	Fingersfun 
 * @link 	www.fingersfun.com 
 * @version 	Version 0.1.0 
 * @developer 	Kiran Kumar.M (kiran@fingersfun.com) 
 *
 * */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       Api.Model
 */
class ApiModel extends ApiAppModel {
}
