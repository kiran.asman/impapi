<?php

App::uses('ApiAppController', 'Api.Controller');
App::uses('Xml', 'Utility');

/**
 * CakePHP ApiController
 * @author Kiran Kumar M
 */
class ApiController extends ApiAppController {

    public function beforeFilter() {

        parent::beforeFilter();
        
            $this->response->header('Access-Control-Allow-Origin','*');
            $this->response->header('Access-Control-Allow-Methods','*');
            // $this->response->header('Access-Control-Allow-Headers','X-Requested-With');
            $this->response->header('Access-Control-Allow-Headers','Content-Type','X-JWT-AUTH');
            $this->response->header('Access-Control-Max-Age','172800');
            //$this->set('_serialize', true);
            $this->response->cors(
                $this->request,
                [$this->request->host()],
                ['PUT', 'GET', 'POST', 'DELETE', 'OPTIONS'],
                ['origin', 'x-requested-with', 'content-type'],
                true
            );
        if ($this->RequestHandler->ext === 'json') {
            $this->response->type('json');
            $this->RequestHandler->ext = 'json';
            //$this->viewClass = 'classes/json';
            //$this->render('/json');
        } else
        if ($this->RequestHandler->ext === 'xml') {
            $this->response->type('xml');
            $this->RequestHandler->ext = 'xml';
            //$this->render('/xml');
        } else {
            throw new BadRequestException();
        }
        
        
    }
}
