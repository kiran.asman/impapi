

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Message from Purohit</title>
        
        
        <style> @media only screen and (max-width: 300px){ 
                body {
                    width:218px !important;
                    margin:auto !important;
                }
                .table {width:195px !important;margin:auto !important;}
                .logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto !important;display: block !important;}      
                span.title{font-size:20px !important;line-height: 23px !important}
                span.subtitle{font-size: 14px !important;line-height: 18px !important;padding-top:10px !important;display:block !important;}        
                td.box p{font-size: 12px !important;font-weight: bold !important;}
                .table-recap table, .table-recap thead, .table-recap tbody, .table-recap th, .table-recap td, .table-recap tr { 
                    display: block !important; 
                }
                .table-recap{width: 200px!important;}
                .table-recap tr td, .conf_body td{text-align:center !important;}    
                .address{display: block !important;margin-bottom: 10px !important;}
                .space_address{display: none !important;}   
            }
    @media only screen and (min-width: 301px) and (max-width: 500px) { 
                body {width:308px!important;margin:auto!important;}
                .table {width:285px!important;margin:auto!important;}   
                .logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}    
                .table-recap table, .table-recap thead, .table-recap tbody, .table-recap th, .table-recap td, .table-recap tr { 
                    display: block !important; 
                }
                .table-recap{width: 295px !important;}
                .table-recap tr td, .conf_body td{text-align:center !important;}
                
            }
    @media only screen and (min-width: 501px) and (max-width: 768px) {
                body {width:478px!important;margin:auto!important;}
                .table {width:450px!important;margin:auto!important;}   
                .logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}            
            }
    @media only screen and (max-device-width: 480px) { 
                body {width:308px!important;margin:auto!important;}
                .table {width:285px;margin:auto!important;} 
                .logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}
                
                .table-recap{width: 295px!important;}
                .table-recap tr td, .conf_body td{text-align:center!important;} 
                .address{display: block !important;margin-bottom: 10px !important;}
                .space_address{display: none !important;}   
            }
</style>

    </head>
    <body style="-webkit-text-size-adjust:none;background-color:#f9f9f9;max-width: 100%;
    width: 100%!important;font-family:Open-sans, sans-serif;color:#555454;font-size:13px;line-height:18px;margin:auto">
        <table class="table table-mail" style="width:100%;margin-top:10px;-moz-box-shadow:0 0 5px #afafaf;-webkit-box-shadow:0 0 5px #afafaf;-o-box-shadow:0 0 5px #afafaf;box-shadow:0 0 5px #afafaf;filter:progid:DXImageTransform.Microsoft.Shadow(color=#afafaf,Direction=134,Strength=5)">
            <tr>
                <td class="space" style="width:85px;padding:7px 0">&nbsp;</td>
                <td align="center" style="padding:7px 0">
                    <table class="table" bgcolor="#f9f9f9" style="width:100%">
                        <tr>
                            <?php // Router::fullbaseUrl().'/img/uploads/profile_pics/default_user.png ?>
                            <td align="center" class="logo" style="padding:7px 0">
                                <a title="<?php echo $merchant_name; ?>" href="<?php echo Router::fullbaseUrl().'/admin/dist'; ?>" style="color:#337ff1">
                                    <img style="width:300px" src="<?php echo Router::fullbaseUrl().'/img/logo.png'; ?>" alt="<?php echo $merchant_name; ?>" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="box" style="padding:7px 0">
                                <table class="table" style="background-color: white;width:100%;padding: 32px;">
                                    <tr>
                                        <td width="10" style="padding:7px 0">&nbsp;</td>
                                        <td style="padding:7px 0">
                                            <font size="2" face="Open-sans, sans-serif" color="#555454">
                                                <p data-html-only="1" style="font-size:25px;color: #4E80BC;line-height: 30px;margin-bottom: 12px;margin: 0 0 12px;font-weight:bold;">
                                                    Join <?php echo $merchant_name; ?> on SecureID</p>
                                                <div style="font-size: 17px;line-height: 24px;margin: 0 0 16px;">
                                                    You have been invited to SecureID, a cure for most cyber security threats and frauds - Man-in-the-middle attacks, Phishing, Smishing, Identity thefts et al. <a href="https://disruptiveapp.com/secure-id-identity-and-approval-management-platform">Learn more.</a><br />
                                                    <br /> 
                                                    Download the app and start using it.<br />
                                                    <table class="table" align="center" style="width:50%">
                                                    <tr>
                                                        <td align="center" class="logo" style="padding:7px 0">
                                                            <a href="https://play.google.com/store/apps/details?id=com.daab.secureid"  target="_blank">
                                                            <!--<i class="fa fa-android" aria-hidden="true"></i>Android-->
                                                            <img src="<?php echo Router::fullbaseUrl().'/img/andriodicon.png'; ?>" alt="SecureID Android App" style="border:0;">
                                                        </a>
                                                        </td>
                                                        <td align="center" class="logo" style="padding:7px 0">
                                                            <a href="https://itunes.apple.com/us/app/secureid-stay-secure/id1273770292?mt=8"  target="_blank">
                                                            <!--<i class="fa fa-apple" aria-hidden="true"></i>Apple -->
                                                            <img src="<?php echo Router::fullbaseUrl().'/img/appleicon.png'; ?>" alt="SecureID iOS App" style="border:0;">
                                                        </a>
                                                        </td>
                                                    </tr>
                                                    </table>
                                                </div>
                                            </font>
                                        </td>
                                        <td width="10" style="padding:7px 0">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td class="space_footer" style="padding:0!important">&nbsp;</td>
                        </tr>
                    </table>
                </td>
                <td class="space" style="width:85px;padding:7px 0">&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
