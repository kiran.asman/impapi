
<?php

if(!empty($errors)){
    echo json_encode(compact("status","message","content","errors"), JSON_PRETTY_PRINT);
}else{
    if(isset($this->request->params['paging']) && !empty($this->request->params['paging'])){
    $paginginfo = $this->Paginator->params();
    //$pagination['has_next_page'] =  $this->Paginator->hasNext();
    if($this->Paginator->hasNext()){
        $pagination['next_page'] =  $paginginfo['nextPage'];
    }
    if($this->Paginator->hasPrev()){
        $pagination['prev_page'] =  $paginginfo['prevPage'];
    }
    $pagination['records_count'] =  $paginginfo['count'];
    $pagination['pages_count'] =  $paginginfo['pageCount'];
    //$pagination['has_prev_page'] =  $this->Paginator->hasPrev();
    $pagination['current_page']  =  (int)$this->Paginator->current();
            //print_r("Pagination.......");
            //echo json_encode(compact("status","message","content", "pagination"), JSON_PRETTY_PRINT);
            echo json_encode(compact("status","message","content", "paginginfo"), JSON_PRETTY_PRINT);
    }else{
            $paginginfo = null;
            //print_r("Not Pagination.......");
            echo json_encode(compact("status","message","content"), JSON_PRETTY_PRINT);
    }
}

?> 
