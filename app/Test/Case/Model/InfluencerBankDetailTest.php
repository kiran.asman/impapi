<?php
App::uses('InfluencerBankDetail', 'Model');

/**
 * InfluencerBankDetail Test Case
 */
class InfluencerBankDetailTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.influencer_bank_detail',
		'app.influencer'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->InfluencerBankDetail = ClassRegistry::init('InfluencerBankDetail');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->InfluencerBankDetail);

		parent::tearDown();
	}

}
