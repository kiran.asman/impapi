<?php
App::uses('InfluencerCategory', 'Model');

/**
 * InfluencerCategory Test Case
 */
class InfluencerCategoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.influencer_category',
		'app.influencer',
		'app.category'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->InfluencerCategory = ClassRegistry::init('InfluencerCategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->InfluencerCategory);

		parent::tearDown();
	}

}
