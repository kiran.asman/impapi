<?php
App::uses('SocialProfile', 'Model');

/**
 * SocialProfile Test Case
 */
class SocialProfileTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.social_profile',
		'app.influencer',
		'app.user',
		'app.influencer_bank_detail',
		'app.influencer_campaign',
		'app.campaign',
		'app.agency',
		'app.notification',
		'app.notification_status',
		'app.notification_type',
		'app.campaign_status',
		'app.campaign_doc',
		'app.doc_type',
		'app.influencer_category',
		'app.category',
		'app.social_network'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SocialProfile = ClassRegistry::init('SocialProfile');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SocialProfile);

		parent::tearDown();
	}

}
