<?php
App::uses('NotificationStatus', 'Model');

/**
 * NotificationStatus Test Case
 */
class NotificationStatusTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.notification_status',
		'app.notification'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->NotificationStatus = ClassRegistry::init('NotificationStatus');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->NotificationStatus);

		parent::tearDown();
	}

}
