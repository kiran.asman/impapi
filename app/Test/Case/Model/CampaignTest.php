<?php
App::uses('Campaign', 'Model');

/**
 * Campaign Test Case
 */
class CampaignTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.campaign',
		'app.agency',
		'app.user',
		'app.notification',
		'app.campaign_status',
		'app.influencer_campaign',
		'app.campaign_doc',
		'app.doc_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Campaign = ClassRegistry::init('Campaign');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Campaign);

		parent::tearDown();
	}

}
