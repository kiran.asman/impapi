<?php
App::uses('Influencer', 'Model');

/**
 * Influencer Test Case
 */
class InfluencerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.influencer',
		'app.user',
		'app.influencer_bank_detail',
		'app.influencer_campaign',
		'app.campaign',
		'app.agency',
		'app.notification',
		'app.campaign_status',
		'app.campaign_doc',
		'app.doc_type',
		'app.influencer_category',
		'app.category',
		'app.social_profile'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Influencer = ClassRegistry::init('Influencer');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Influencer);

		parent::tearDown();
	}

}
