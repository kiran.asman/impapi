<?php
App::uses('DocType', 'Model');

/**
 * DocType Test Case
 */
class DocTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.doc_type',
		'app.campaign_doc',
		'app.campaign',
		'app.agency',
		'app.user',
		'app.notification',
		'app.campaign_status',
		'app.influencer_campaign'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DocType = ClassRegistry::init('DocType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DocType);

		parent::tearDown();
	}

}
