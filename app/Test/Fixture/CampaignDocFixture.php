<?php
/**
 * CampaignDoc Fixture
 */
class CampaignDocFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'doc_type_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'doc_size' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => false),
		'doc_mime_type' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'doc' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 350, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'is_active' => array('type' => 'boolean', 'null' => false, 'default' => '1'),
		'campaign_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'doc_type_id' => 1,
			'doc_size' => '',
			'doc_mime_type' => 'Lorem ipsum dolor sit amet',
			'doc' => 'Lorem ipsum dolor sit amet',
			'created' => '2018-12-04 06:33:11',
			'is_active' => 1,
			'campaign_id' => ''
		),
	);

}
