<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP AgenciesController
 * @author srikanth
 */
class AgenciesController extends AppController {
    
    public function beforeFilter() {
        parent::beforeFilter();
        
    }
    
    /**
     * @method getAgencies
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/agencies/getAgencies/
     * REQUEST :  NULL
     * METHOD : GET or POST    
     * RESPONSE SUCCESS :
     * {
            "status": "SUCCESS",
            "message": "agencies found",
            "content": [
                {
                "id": "1",
                "user_id": "4",
                "email": "mb@gmail.com",
                "logo": "mb.png",
                "first_name": "Mahesh",
                "last_name": "G",
                "brand_name": "MBCinemas",
                "designation": "Developer",
                "gender": "Male",
                "phone_no": "7894561230",
                "city": "Hyderabad",
                "created": "2018-11-30 12:05:08",
                "modified": "2018-11-30 12:05:08"
               }
     * 
     */

    public function api_1_0_getAgencies() {
        
        if ($this->request->is('post')) {
                $requesteddata = $this->request->data;
            }
            if ($this->request->is('get')) {
                $requesteddata = $this->request->params['named'];
            }
            if(isset($requesteddata['page'])){
                $page = $requesteddata['page'];
            }else{
               $page = 1; 
            }
            if(isset($requesteddata['limit'])){
                $limit = $requesteddata['limit'];
            }else{
                $limit = 10;
            }
            $this->loadModel('Agency');
                $this->loadModel('User');
                $this->loadModel('AgencyCategory');
                $this->loadModel('Notification');
                
                $this->User->unbindModel(
                            array('hasOne' => array('Agency','Influencer','Admin'))
                        );
                $this->AgencyCategory->unbindModel(
                            array('belongsTo' => array('Campaign','Notification','Agency'))
                        );
                $this->Agency->unbindModel(
                            array('hasMany' => array('Agency','Campaign','Notification'))
                        );
        $this->paginate = array(
            'page' => $page,
            'limit' => $limit, 
            'conditions' => array(
            'User.is_active' => ACTIVE,
            ),
            'recursive' => 2,
                'order' => array('Agency.id' => 'desc')
        );
        $this->loadModel('Agency');
        $agencies = $this->paginate('Agency');
        $agencies = Set::extract('/Agency/.', $agencies);
       // echo"<pre>";print_r($agencies);"</pre>";exit;
        
        if ($agencies) {
            $message = 'agencies found';
            $status = 'SUCCESS';
            $content = $agencies;
        } else {
            $status = 'SUCCESS';
            $message = 'agencies not found';
            $content = $agencies;
        }
        //pr($influencers);
        $pagination = $this->request->params['paging']['Agency'];
        unset($pagination['order']);
        unset($pagination['options']);
        //$this->set(compact('services',$services));
        
        $this->set([
            'pagination' => $pagination,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content', 'pagination']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

}
