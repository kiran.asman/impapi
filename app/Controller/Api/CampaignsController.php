<?php

App::uses('ApiController', 'Api.Controller');

class CampaignsController extends ApiController {

    /**
     * Components
     *
     * @var array
     * */
    public $components = array('Paginator', 'RequestHandler',);

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /**
     * @method createCampaign
     * 
     * @URL: http://localhost:90/impapi/api/1.0/json/campaigns/createCampaign/
     * REQUEST :
     * 
     *  {
     *      
     *      "user_id": "28"
     *  }
     * METHOD : POST    
     * 
     * RESPONSE SUCCESS:
     *  {
           "status": "Success",
           "message": "Campaign created successfully.",
           "content": {
           "agency_id": "11",
           "campaign_name": "Iphone10x",
           "brand_name": "Apple",
           "hash_tags": "#Apple#iphone",
           "submission_date": "2018-12-15",
           "campaign_id": "16"
}
}
     * 
     */
    public function api_1_0_createCampaign() {

        if ($this->request->is('post')) {
            $this->layout = 'json';
            $requesteddata = $this->request->data;
            $user_id = $requesteddata['user_id'];
            $options = array(
                'conditions' => array(
                    'User.is_active' => ACTIVE,
                    'User.id' => $user_id,
                    'User.user_role' => 'AGENCY',
                ), 'recursive' => 0);
            $this->loadModel('User');
            $this->loadModel('Campaign');
            $userdata = $this->User->find('first', $options);
            if (!empty($userdata)) {

                $requesteddata['agency_id'] = $userdata['Agency']['id'];
                if ($this->Campaign->Save($requesteddata)) {
                    $campaign_id = $this->Campaign->id;

                    /* Genders */
                    if (!empty($requesteddata['gender_ids'])) {
                        $this->loadModel('CampaignGender');
                        $genderData = [];
                        $genderIds = $requesteddata['gender_ids'];
                        foreach ($genderIds as $gid) {
                            $gender['CampaignGender']['campaign_id'] = $campaign_id;
                            $gender['CampaignGender']['gender_id'] = $gid;
                            $genderData[] = $gender;
                        }
                        $this->CampaignGender->SaveAll($genderData);
                    }

                    /* InfluencerTypes */
                    if (!empty($requesteddata['influencer_type_ids'])) {
                        $this->loadModel('CampaignInfluencerType');
                        $influencerTypeData = [];
                        $influencerTypeIds = $requesteddata['influencer_type_ids'];
                        foreach ($influencerTypeIds as $infid) {
                            $inf['CampaignInfluencerType']['campaign_id'] = $campaign_id;
                            $inf['CampaignInfluencerType']['influencer_type_id'] = $infid;
                            $influencerTypeData[] = $inf;
                        }
                        $this->CampaignInfluencerType->SaveAll($influencerTypeData);
                    }

                    /* Goals */
                    if (!empty($requesteddata['goal_ids'])) {
                        $this->loadModel('CampaignGoal');
                        $goalData = [];
                        $goalIds = $requesteddata['goal_ids'];
                        foreach ($goalIds as $goalid) {
                            $goal['CampaignGoal']['campaign_id'] = $campaign_id;
                            $goal['CampaignGoal']['goal_id'] = $goalid;
                            $goalData[] = $goal;
                        }
                        $this->CampaignGoal->SaveAll($goalData);
                    }

                    /* Categories */
                    if (!empty($requesteddata['category_ids'])) {
                        $this->loadModel('CampaignCategory');
                        $categoryData = [];
                        $categoryIds = $requesteddata['category_ids'];
                        foreach ($categoryIds as $categoryId) {
                            $category['CampaignCategory']['campaign_id'] = $campaign_id;
                            $category['CampaignCategory']['category_id'] = $categoryId;
                            $categoryData[] = $category;
                        }
                        $this->CampaignCategory->SaveAll($categoryData);
                    }

                    /* SocialNetworks */
                    if (!empty($requesteddata['deliverable_ids'])) {

                        $this->loadModel('CampaignDeliverable');
                        $DeliverableData = [];
                        $DeliverableIds = json_decode(json_encode($requesteddata['deliverable_ids']), true);
                        foreach ($DeliverableIds as $result) {

                            $Did['CampaignDeliverable']['campaign_id'] = $campaign_id;
                            $Did['CampaignDeliverable']['deliverable_id'] = $result['deliverable_id'];
                            $Did['CampaignDeliverable']['count'] = $result['count'];
                            $Did['CampaignDeliverable']['content'] = $result['content'];
                            $DeliverableData[] = $Did;
                        }
                        $this->CampaignDeliverable->SaveAll($DeliverableData);
                    }

                    /* Rewards 
                    if (!empty($requesteddata['reward_type'])) {
                        if ($requesteddata['reward_type'] == PAID) {
                            $rewardData['CampaignReward']['campaign_id'] = $campaign_id;
                            $rewardData['CampaignReward']['reward_type'] = $requesteddata['reward_type'];
                            $rewardData['CampaignReward']['amount'] = $requesteddata['amount'];
                        }
                        if ($requesteddata['reward_type'] == BARTER) {
                            $rewardData['CampaignReward']['campaign_id'] = $campaign_id;
                            $rewardData['CampaignReward']['reward_type'] = $requesteddata['reward_type'];
                            $rewardData['CampaignReward']['reason'] = $requesteddata['reason'];
                        }
                        $this->loadModel('CampaignReward');
                        $this->CampaignReward->Save($rewardData);
                    } */

                    /* AgeGroups */
                    if (!empty($requesteddata['age_group_ids'])) {
                        $this->loadModel('CampaignAgeGroup');
                        $AgeGroupData = [];
                        $AgeGroupIds = $requesteddata['age_group_ids'];
                        foreach ($AgeGroupIds as $AgeGroupId) {
                            $age['CampaignAgeGroup']['campaign_id'] = $campaign_id;
                            $age['CampaignAgeGroup']['age_group_id'] = $AgeGroupId;
                            $AgeGroupData[] = $age;
                        }
                        $this->CampaignAgeGroup->SaveAll($AgeGroupData);
                    }
                    $dos = [];
                    $donts = [];
                    if (!empty($requesteddata['dos']) || !empty($requesteddata['donts'])){                        
                        if (!empty($requesteddata['dos'])){
                            $dos = $requesteddata['dos'];
                        }
                        if (!empty($requesteddata['donts'])){
                            $donts = $requesteddata['donts'];
                        }                        
                        $RuleData['dos'] = implode(',', $dos);
                        $RuleData['donts'] = implode(',', $donts);
                        $RuleData['campaign_id'] = $campaign_id;
                        $this->loadModel('CampaignRule');
                        $this->CampaignRule->Save($RuleData);
                    }
                    /* Locations */
                    if (!empty($requesteddata['location_ids'])) {
                        $this->loadModel('CampaignLocation');
                        $locationData = [];
                        $locationIds = $requesteddata['location_ids'];
                        foreach ($locationIds as $locationId) {
                            $location['CampaignLocation']['campaign_id'] = $campaign_id;
                            $location['CampaignLocation']['location_id'] = $locationId;
                            $locationData[] = $location;
                        }
                        $this->CampaignLocation->SaveAll($locationData);
                    }


                    $status = 'Success';
                    $message = 'Campaign created successfully.';
                    $content = $requesteddata;
                } else {
                    $status = 'Failed';
                    $message = 'Campaign could not be craeted, please try again.';
                    $content = $requesteddata;
                }
            } else {
                $status = 'Failed';
                $message = 'Invalid Agencie.';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    /**
     * @method updateCamapign
     * 
     * @URL: http://192.168.0.22/mvp/api/1.0/json/campaigns/updateCampaign/
     * REQUEST :
     * 
     *  {
     *      "id":16,
     *       "campaign_name":"Sports",
     *       "brand_name":"Sports description"
     *   }  
     *     
     *  
     * METHOD : PUT    
     * 
     * RESPONSE SUCCESS:
     * {
      "status": "SUCCESS",
      "message": "Campaign updated.",
      "content": {
      "id": 10,
      "category": "Sports",
      "description": "Sports description"
      }
      }
     * 
     */
    public function api_1_0_updateCampaign() {

        if ($this->request->is('put')) {
            $this->layout = 'json';
            $requesteddata = $this->request->data;
            $isCampaignExist = $this->Campaign->find('first', array('conditions' => array(
                    'Campaign.id' => $requesteddata['id']
            )));
            if (!empty($isCampaignExist)) {

                $this->loadModel('Campaign');
                $campaign_id = $isCampaignExist['Campaign']['id'];
                //$this->Campaign->set($requesteddata);
                $this->Campaign->id = $campaign_id;
                $this->Campaign->Save($requesteddata);
                //$this->Campaign->id = $campaign_id;
                if (!empty($campaign_id)) {
                    /* AgeGroups */
                    if (!empty($requesteddata['age_group_ids']) && is_array($requesteddata['age_group_ids'])) {
                        $this->loadModel('CampaignAgeGroup');
                        $AgeGroupData = [];
                        $AgeGroupIds = $requesteddata['age_group_ids'];
                        $this->CampaignAgeGroup->deleteAll(array('CampaignAgeGroup.campaign_id' => $campaign_id), false);

                        foreach ($AgeGroupIds as $AgeGroupId) {
                            $age['CampaignAgeGroup']['campaign_id'] = $campaign_id;
                            $age['CampaignAgeGroup']['age_group_id'] = $AgeGroupId;
                            $AgeGroupData[] = $age;
                        }
                        $this->CampaignAgeGroup->SaveAll($AgeGroupData);
                    }

                    /* Categories */
                    if (!empty($requesteddata['category_ids']) && is_array($requesteddata['category_ids'])) {
                        $this->loadModel('CampaignCategory');
                        $this->CampaignCategory->deleteAll(array('CampaignCategory.campaign_id' => $campaign_id), false);
                        $categoryData = [];
                        $categoryIds = $requesteddata['category_ids'];
                        foreach ($categoryIds as $categoryId) {
                            $category['CampaignCategory']['campaign_id'] = $campaign_id;
                            $category['CampaignCategory']['category_id'] = $categoryId;
                            $categoryData[] = $category;
                        }
                        $this->CampaignCategory->SaveAll($categoryData);
                    }

                    /* Deliverables */
                    if (!empty($requesteddata['deliverable_ids']) && is_array($requesteddata['deliverable_ids'])) {

                        $this->loadModel('CampaignDeliverable');
                        $this->CampaignDeliverable->deleteAll(array('CampaignDeliverable.campaign_id' => $campaign_id), false);
                        $DeliverableData = [];
                        $DeliverableIds = json_decode(json_encode($requesteddata['deliverable_ids']), true);
                        foreach ($DeliverableIds as $result) {

                            $Did['CampaignDeliverable']['campaign_id'] = $campaign_id;
                            $Did['CampaignDeliverable']['deliverable_id'] = $result['deliverable_id'];
                            $Did['CampaignDeliverable']['count'] = $result['count'];
                            $Did['CampaignDeliverable']['content'] = $result['content'];
                            $DeliverableData[] = $Did;
                        }
                        $this->CampaignDeliverable->SaveAll($DeliverableData);
                    }

                    /* Genders */
                    if (!empty($requesteddata['gender_ids']) && is_array($requesteddata['gender_ids'])) {
                        $this->loadModel('CampaignGender');
                        $this->CampaignGender->deleteAll(array('CampaignGender.campaign_id' => $campaign_id), false);
                        $genderData = [];
                        $genderIds = $requesteddata['gender_ids'];
                        foreach ($genderIds as $gid) {
                            $gender['CampaignGender']['campaign_id'] = $campaign_id;
                            $gender['CampaignGender']['gender_id'] = $gid;
                            $genderData[] = $gender;
                        }
                        $this->CampaignGender->SaveAll($genderData);
                    }

                    /* Goals */
                    if (!empty($requesteddata['goal_ids']) && is_array($requesteddata['goal_ids'])) {
                        $this->loadModel('CampaignGoal');
                        $this->CampaignGoal->deleteAll(array('CampaignGoal.campaign_id' => $campaign_id), false);
                        $goalData = [];
                        $goalIds = $requesteddata['goal_ids'];
                        foreach ($goalIds as $goalid) {
                            $goal['CampaignGoal']['campaign_id'] = $campaign_id;
                            $goal['CampaignGoal']['goal_id'] = $goalid;
                            $goalData[] = $goal;
                        }
                        $this->CampaignGoal->SaveAll($goalData);
                    }



                    /* InfluencerTypes */
                    if (!empty($requesteddata['influencer_type_ids']) && is_array($requesteddata['influencer_type_ids'])) {
                        $this->loadModel('CampaignInfluencerType');
                        $this->CampaignInfluencerType->deleteAll(array('CampaignInfluencerType.campaign_id' => $campaign_id), false);
                        $influencerTypeData = [];
                        $influencerTypeIds = $requesteddata['influencer_type_ids'];
                        foreach ($influencerTypeIds as $infid) {
                            $inf['CampaignInfluencerType']['campaign_id'] = $campaign_id;
                            $inf['CampaignInfluencerType']['influencer_type_id'] = $infid;
                            $influencerTypeData[] = $inf;
                        }
                        $this->CampaignInfluencerType->SaveAll($influencerTypeData);
                    }

                    /* Locations */
                    if (!empty($requesteddata['location_ids']) && is_array($requesteddata['location_ids'])) {
                        $this->loadModel('CampaignLocation');
                        $this->CampaignLocation->deleteAll(array('CampaignLocation.campaign_id' => $campaign_id), false);
                        $locationData = [];
                        $locationIds = $requesteddata['location_ids'];
                        foreach ($locationIds as $locationId) {
                            $location['CampaignLocation']['campaign_id'] = $campaign_id;
                            $location['CampaignLocation']['location_id'] = $locationId;
                            $locationData[] = $location;
                        }
                        $this->CampaignLocation->SaveAll($locationData);
                    }

                    /* Rewards */
                    if (!empty($requesteddata['reward_type'])) {
                        $this->loadModel('CampaignReward');
                        $this->CampaignReward->deleteAll(array('CampaignReward.campaign_id' => $campaign_id), false);
                        if ($requesteddata['reward_type'] == PAID) {
                            $rewardData['CampaignReward']['campaign_id'] = $campaign_id;
                            $rewardData['CampaignReward']['reward_type'] = $requesteddata['reward_type'];
                            $rewardData['CampaignReward']['amount'] = $requesteddata['amount'];
                        }
                        if ($requesteddata['reward_type'] == BARTER) {
                            $rewardData['CampaignReward']['campaign_id'] = $campaign_id;
                            $rewardData['CampaignReward']['reward_type'] = $requesteddata['reward_type'];
                            $rewardData['CampaignReward']['reason'] = $requesteddata['reason'];
                        }

                        $this->CampaignReward->Save($rewardData);
                    }

                    /* Rules */
                    $dos = [];
                    $donts = [];
                    if (!empty($requesteddata['dos']) || !empty($requesteddata['donts'])) {
                        if (!empty($requesteddata['dos'])) {
                            $dos = $requesteddata['dos'];
                        }
                        if (!empty($requesteddata['donts'])) {
                            $donts = $requesteddata['donts'];
                        }
                        $RuleData['dos'] = implode(',', $dos);
                        $RuleData['donts'] = implode(',', $donts);
                        $RuleData['campaign_id'] = $campaign_id;
                        $this->loadModel('CampaignRule');
                        $this->CampaignRule->set($requesteddata);
                        $this->CampaignRule->Save($RuleData);
                    }



                    $status = 'SUCCESS';
                    $message = 'Campaign updated.';
                    $content = $requesteddata;
                } else {
                    $status = 'ERROR';
                    $message = 'influencer update failed';
                    $content = $requesteddata;
                }
            } else {
                $status = 'Failed';
                $message = 'Campaign does not exist.';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    /**
     * @method getCamapigns
     * 
     * URL: http://192.168.0.22/mvp/api/1.0/json/campaigns/getCampaigns/
     * REQUEST :
     * 
     * METHOD : POST (or) GET
     *  
     *    REQUEST :POST
     *
     *    URL:http://192.168.0.22/mvp/api/1.0/json/campaigns/getCampaigns/
     *        
     *          {
      user_id:28,
      member_id:12,
      member_type:AGENCY(or)(INFLUENCER)(or)(ADMIN)
      }
     *      
     * RESPONSE SUCCESS:
      {
      "id": "17",
      "agency_id": "12",
      "campaign_name": "RedmiNote6 Promotion",
      "brand_name": "Redmi",
      "hash_tags": "#Redmi #Note6 #Mi",
      "content_guidelines": "Promote our New Brand with Tamada media videos",
      "sample_content": "The Wait is over RedmiNote6 just arrived ",
      "last_date_to_apply": "2018-12-12",
      "start_date": "2018-12-15",
      "submission_date": "2018-12-21",
      "purpose": "Product Launch",
      "campaign_type": "CONTENT",
      "is_active": true,
      "created": "2018-12-06 06:40:50",
      "modified": "2018-12-06 06:40:50",
      "campaign_status_id": "3"
      }  

     *  REQUEST :GET

     * http://192.168.0.22/mvp/api/1.0/json/campaigns/getCampaigns/user_id:28/member_id:12/member_type:AGENCY

     */
    public function api_1_0_getCampaigns() {
        
        $content = NULL;
        $pagination = null;     
        if ($this->request->is(array('post', 'get'))) {
            /*
              if(!empty($this->getTokenInfo())){
              $requesteddata = $this->getTokenInfo();
              }
             */
            if ($this->request->is('post')) {
                $requesteddata = $this->request->data;
            }
            if ($this->request->is('get')) {
                $requesteddata = $this->request->params['named'];
            }
            if (isset($requesteddata['page'])) {
                $page = $requesteddata['page'];
            } else {
                $page = 1;
            }
            if (isset($requesteddata['limit'])) {
                $limit = $requesteddata['limit'];
            } else {
                $limit = 10;
            }

            if (!empty($requesteddata)) {
                $fields = array(
                    'Campaign.id',
                    'Campaign.agency_id',
                    'Campaign.campaign_name',
                    'Campaign.brand_name',
                    'Campaign.product_name',
                    'Campaign.hash_tags',
                 //   'Campaign.reward',
                    'Campaign.objective',
                    'Campaign.content_guidelines',
                    'Campaign.sample_content',
                    'Campaign.last_date_to_apply',
                    'Campaign.start_date',
                    'Campaign.submission_date',
                    'Campaign.is_active',
                    'Campaign.total_budget',
                    'Campaign.created',
                    'Campaign.modified',
                    'Campaign.campaign_status_id',
                    'CampaignStatus.id',
                    'CampaignStatus.status',
                    'Agency.logo',
                    'Agency.brand_name'
                );
                if ($requesteddata['member_type'] == ADMIN) {

                    $conditions = [];
                    if (isset($requesteddata['status_id'])) {
                        $campaignStatus = $requesteddata['status_id'];
                        $conditions = array('Campaign.is_active' => ACTIVE, 'Campaign.campaign_status_id' => $campaignStatus);
                    } else {
                        $conditions = array('Campaign.is_active' => ACTIVE);
                    }
                    $this->paginate = array(
                        'page' => $page,
                        'limit' => $limit,
                        'fields' => $fields,
                        'conditions' => $conditions,
                        'order' => array(
                            'Campaign.id' => 'desc'
                        ),
                        'recursive' => 3
                    );


                    $this->loadModel('Campaign');
                    $this->loadModel('CampaignDeliverable');
                    $this->loadModel('CampaignStatus');
                    $this->loadModel('CampaignGoal');
                    $this->loadModel('CampaignGender');
                    $this->loadModel('CampaignAgeGroup');
                    $this->loadModel('CampaignInfluencerType');
                    $this->loadModel('CampaignCategory');
                    $this->loadModel('CampaignLocation');
                    $this->loadModel('CampaignRule');
                    $this->loadModel('CampaignReward');
                    $this->loadModel('Deliverable');
                    $this->loadModel('SocialNetwork');
                    $this->loadModel('Goal');
                    // $this->loadModel('CampaignGoal');
                    $this->loadModel('CampaignFilter');
                    $this->loadModel('Gender');
                    $this->loadModel('AgeGroup');
                    $this->loadModel('InfluencerType');
                    $this->loadModel('Location');
                    $this->loadModel('Agency');

                    $this->Campaign->bindModel(
                            array('hasMany' => array('CampaignDeliverable'))
                    );

                    $this->Campaign->unbindModel(
                            array('hasMany' => array('Notification', 'CampaignDoc', 'InfluencerCampaign'))
                    );
                    $this->CampaignStatus->unbindModel(
                            array('hasMany' => array('Campaign', 'InfluencerCampaign'))
                    );
                    $this->CampaignGoal->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignGender->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignAgeGroup->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignInfluencerType->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignCategory->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignLocation->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignRule->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignReward->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignDeliverable->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->Goal->unbindModel(
                            array('hasMany' => array('CampaignFilter', 'CampaignGoal'))
                    );
                    $this->Gender->unbindModel(
                            array('hasMany' => array('CampaignFilter', 'CampaignGender'))
                    );
                    $this->AgeGroup->unbindModel(
                            array('hasMany' => array('CampaignFilter', 'CampaignAgeGroup'))
                    );
                    $this->InfluencerType->unbindModel(
                            array('hasMany' => array('CampaignFilter', 'CampaignInfluencerType'))
                    );
                    $this->Location->unbindModel(
                            array('hasMany' => array('CampaignLocation'))
                    );
                    $this->Deliverable->unbindModel(
                            array('hasMany' => array('CampaignDeliverable'), 'belongsTo' => array('Campaign'))
                    );
                    $this->Agency->unbindModel(
                            array('hasMany' => array('Campaign', 'AgencyCategory', 'Notification'), 'belongsTo' => array('User', 'Gender'))
                    );


                    $campaigns = $this->paginate('Campaign');
                    // echo"<pre>";print_r($campaigns);"</pre>";exit;
                    //$campaigns = Set::extract('/Campaign/.', $campaigns);

                    if ($campaigns) {
                        $message = 'Campaigns found';
                        $status = 'SUCCESS';
                        $content = $campaigns;
                    } else {
                        $status = 'SUCCESS';
                        $message = 'Campaigns not found';
                        $content = $campaignsdata;
                    }
                    $pagination = $this->request->params['paging']['Campaign'];
                    unset($pagination['order']);
                    unset($pagination['options']);
                    $this->set([
                        'pagination' => $pagination,
                        'status' => $status,
                        'message' => $message,
                        'content' => $content,
                        '_serialize' => ['status', 'message', 'content', 'pagination']
                    ]);
                } else if ($requesteddata['member_type'] == AGENCY) {

                    $this->paginate = array(
                        'page' => $page,
                        'limit' => $limit,
                        'fields' => $fields,
                        'conditions' => array(
                            'Campaign.is_active' => ACTIVE,
                            'Campaign.agency_id' => $requesteddata['member_id'],
                        ),
                        'order' => array(
                            'Campaign.id' => 'desc'
                        ),
                        'recursive' => 3,
                    );
                    $this->loadModel('Campaign');
                    $this->loadModel('Campaign');
                    $this->loadModel('CampaignStatus');
                    $this->loadModel('CampaignGoal');
                    $this->loadModel('CampaignGender');
                    $this->loadModel('CampaignAgeGroup');
                    $this->loadModel('CampaignInfluencerType');
                    $this->loadModel('CampaignCategory');
                    $this->loadModel('CampaignLocation');
                    $this->loadModel('CampaignRule');
                    $this->loadModel('CampaignReward');
                    $this->loadModel('CampaignDeliverable');
                    $this->loadModel('Deliverable');
                    $this->loadModel('SocialNetwork');
                    $this->loadModel('Goal');
                    // $this->loadModel('CampaignGoal');
                    $this->loadModel('CampaignFilter');
                    $this->loadModel('Gender');
                    $this->loadModel('AgeGroup');
                    $this->loadModel('InfluencerType');
                    $this->loadModel('Location');
                    $this->loadModel('Agency');

                    $this->Campaign->bindModel(
                            array('hasMany' => array('CampaignDeliverable', 'CampaignReward'))
                    );
                    $this->Campaign->unbindModel(
                            array('hasMany' => array('Notification', 'CampaignDoc', 'InfluencerCampaign'))
                    );
                    $this->CampaignStatus->unbindModel(
                            array('hasMany' => array('Campaign', 'InfluencerCampaign'))
                    );
                    $this->CampaignGoal->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignGender->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignAgeGroup->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignInfluencerType->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignCategory->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignLocation->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignRule->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignReward->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignDeliverable->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->Goal->unbindModel(
                            array('hasMany' => array('CampaignFilter', 'CampaignGoal'))
                    );
                    $this->Gender->unbindModel(
                            array('hasMany' => array('CampaignFilter', 'CampaignGender'))
                    );
                    $this->AgeGroup->unbindModel(
                            array('hasMany' => array('CampaignFilter', 'CampaignAgeGroup'))
                    );
                    $this->InfluencerType->unbindModel(
                            array('hasMany' => array('CampaignFilter', 'CampaignInfluencerType'))
                    );
                    $this->Location->unbindModel(
                            array('hasMany' => array('CampaignLocation'))
                    );
                    $this->Deliverable->unbindModel(
                            array('hasMany' => array('CampaignDeliverable'), 'belongsTo' => array('Campaign'))
                    );
                    $this->Agency->unbindModel(
                            array('hasMany' => array('Campaign', 'AgencyCategory', 'Notification'), 'belongsTo' => array('User', 'Gender'))
                    );

                    $campaigns = $this->paginate('Campaign');
                    if ($campaigns) {
                        $message = 'Campaigns found';
                        $status = 'SUCCESS';
                        $content = $campaigns;
                    } else {
                        $status = 'SUCCESS';
                        $message = 'Campaigns not found';
                        $content = $campaigns;
                    }
                    //  $pagination = $this->request->params['paging']['Campaign'];
                    //  unset($pagination['order']);
                    //  unset($pagination['options']);
                    $this->set([
                        // 'pagination' => $pagination,
                        'status' => $status,
                        'message' => $message,
                        'content' => $content,
                        '_serialize' => ['status', 'message', 'content']
                    ]);
                } else if ($requesteddata['member_type'] == INFLUENCER) {

                    $options = array(
                        'conditions' => array(
                            'User.is_active' => ACTIVE,
                            'User.id' => $requesteddata['user_id'],
                            'User.user_role' => $requesteddata['member_type'],
                            'Influencer.id' => $requesteddata['member_id']
                        ),
                        'recursive' => 1
                    );

                    $this->loadModel('Influencer');
                    $influencers = $this->Influencer->find('first', $options);

                    $userage = $this->getMemberAge($influencers['Influencer']['dob']);
                    $genderids[] = $influencers['Gender']['id'];

                    $this->loadModel('AgeGroup');
                    $agegroups = $this->AgeGroup->find('all', array(
                        'conditions' => array(
                            'AgeGroup.age_end >=' => $userage,
                            'AgeGroup.age_start <=' => $userage
                        ),
                            )
                    );

                    $influencercategoryids = Set::extract('/InfluencerCategory/category_id', $influencers);
                    $agegroupids = Set::extract('/AgeGroup/id', $agegroups);
                    /*
                      $fild = "campaign.id,
                      campaign.agency_id,
                      campaign.campaign_name,
                      campaign.brand_name,
                      campaign.hash_tags,
                      campaign.content_guidelines,
                      campaign.sample_content,
                      campaign.last_date_to_apply,
                      campaign.start_date,
                      campaign.submission_date,
                      campaign.is_active,
                      campaign.created,
                      campaign.modified,
                      campaign.campaign_status_id";
                      $campaigns = $this->Campaign->query("SELECT ".$fild." FROM `campaigns` as campaign where campaign.is_active = ". ACTIVE ." and campaign.campaign_status_id = ". 2 . " join campaign_categories as cmpc on campaign.id=cmpc.campaign_id and cmpc.category_id in (".implode(',', $influencercategoryids).") join campaign_genders as cmpg on campaign.id = cmpg.campaign_id and cmpg.gender_id in (".implode(',', $genderids).") join campaign_age_groups as cmpa on campaign.id = cmpa.campaign_id and cmpa.age_group_id in (".implode(',', $agegroupids).")");
                     */
                    /*  $this->Campaign->bindModel(array(
                      'hasMany' => array(
                      'CampaignCategory' => array(
                      'conditions' => array('CampaignCategory.category_id' => $influencercategoryids)
                      ),
                      'CampaignGender' => array(
                      'conditions' => array('CampaignGender.gender_id' => $genderids)
                      ),
                      'CampaignAgeGroup' => array(
                      'conditions' => array(
                      'CampaignAgeGroup.age_group_id' => $agegroupids
                      )
                      )
                      )
                      )
                      ); */

                    $this->loadModel('InfluencerCampaign');
                    $campaign_ids = $this->InfluencerCampaign->find('all', array(
                        'conditions' => array('InfluencerCampaign.influencer_id' => $requesteddata['member_id'],
                            'InfluencerCampaign.campaign_status_id !=' => 4)));

                    /*   $campaign_ids = $this->InfluencerCampaign->find('all', array(
                      'conditions' => array('AND' => array(
                      array('InfluencerCampaign.influencer_id' => $requesteddata['member_id'],
                      array('InfluencerCampaign.campaign_status_id' == 5)
                      ))))); */

                    //echo"<pre>";print_r($campaign_ids);"</pre>";exit;

                    $result = Set::classicExtract($campaign_ids, '{n}.InfluencerCampaign.campaign_id');
                    $current_date = date("Y-m-d");
                    // echo"<pre>";print_r($result);"</pre>";exit;
                    $fields = array(
                        'Campaign.id',
                        'Campaign.agency_id',
                        'Campaign.campaign_name',
                        'Campaign.brand_name',
                        'Campaign.product_name',
                        'Campaign.objective',
                       // 'Campaign.reward',
                        'Campaign.hash_tags',
                        'Campaign.content_guidelines',
                        'Campaign.sample_content',
                        'Campaign.last_date_to_apply',
                        'Campaign.start_date',
                        'Campaign.submission_date',
                        'Campaign.is_active',
                        'Campaign.total_budget',
                        'Campaign.created',
                        'Campaign.modified',
                        'Agency.logo',
                        'Agency.brand_name'
                            //'Campaign.campaign_status_id',
                    );
                    $this->loadModel('Campaign');
                    $this->loadModel('Campaign');
                    $this->loadModel('CampaignStatus');
                    $this->loadModel('CampaignGoal');
                    $this->loadModel('CampaignGender');
                    $this->loadModel('CampaignAgeGroup');
                    $this->loadModel('CampaignInfluencerType');
                    $this->loadModel('CampaignCategory');
                    $this->loadModel('CampaignLocation');
                    $this->loadModel('CampaignRule');
                    $this->loadModel('CampaignReward');
                    $this->loadModel('CampaignDeliverable');
                    $this->loadModel('Deliverable');
                    $this->loadModel('SocialNetwork');
                    $this->loadModel('Goal');
                    // $this->loadModel('CampaignGoal');
                    $this->loadModel('CampaignFilter');
                    $this->loadModel('Gender');
                    $this->loadModel('AgeGroup');
                    $this->loadModel('InfluencerType');
                    $this->loadModel('Location');
                    $this->loadModel('Agency');

                    $this->loadModel('Influencer');

                    $this->Campaign->bindModel(
                            array('hasMany' => array('CampaignDeliverable', 'CampaignReward'))
                    );
                    $this->Campaign->bindModel(
                            array('hasMany' => array('InfluencerCampaign'))
                    );
                    $this->Campaign->unbindModel(
                            array('belongsTo' => array('CampaignStatus'), 'hasMany' => array('Notification', 'CampaignDoc'))
                    );
                    $this->CampaignStatus->unbindModel(
                            array('hasMany' => array('Campaign', 'InfluencerCampaign'))
                    );
                    $this->CampaignGoal->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignGender->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignAgeGroup->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignInfluencerType->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignCategory->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignLocation->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignRule->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignReward->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->CampaignDeliverable->unbindModel(
                            array('belongsTo' => array('Campaign'))
                    );
                    $this->Goal->unbindModel(
                            array('hasMany' => array('CampaignFilter', 'CampaignGoal'))
                    );
                    $this->Gender->unbindModel(
                            array('hasMany' => array('CampaignFilter', 'CampaignGender'))
                    );
                    $this->AgeGroup->unbindModel(
                            array('hasMany' => array('CampaignFilter', 'CampaignAgeGroup'))
                    );
                    $this->InfluencerType->unbindModel(
                            array('hasMany' => array('CampaignFilter', 'CampaignInfluencerType'))
                    );
                    $this->Location->unbindModel(
                            array('hasMany' => array('CampaignLocation'))
                    );
                    $this->Deliverable->unbindModel(
                            array('hasMany' => array('CampaignDeliverable'), 'belongsTo' => array('Campaign'))
                    );
                    $this->InfluencerCampaign->unbindModel(
                            array('belongsTo' => array('Campaign', 'Influencer'))
                    );
                    $this->Agency->unbindModel(
                            array('hasMany' => array('Campaign', 'AgencyCategory', 'Notification'), 'belongsTo' => array('User', 'Gender'))
                    );
                    $this->paginate = array(
                        'page' => $page,
                        'limit' => $limit,
                        'fields' => $fields,
                        'conditions' => array(
                            'Campaign.is_active' => ACTIVE,
                            'Campaign.campaign_status_id' => 2,
                            'Campaign.id !=' => $result,
                            'Campaign.last_date_to_apply >= ' => $current_date
                        ),
                        'order' => array(
                            'Campaign.id' => 'desc'
                        ),
                        'recursive' => 3
                    );

                    $campaigns = $this->paginate('Campaign');
                    $recamp = [];
                    if (!empty($campaigns)) {
                        foreach ($campaigns as $campaign) {
                            $tmpicms = [];
                            if (!empty($campaign['InfluencerCampaign'])) {

                                foreach ($campaign['InfluencerCampaign'] as $inCam) {
                                    if ($inCam['influencer_id'] == $requesteddata['member_id']) {
                                        $tmpicms[] = $inCam;
                                    }
                                }
                            }
                            unset($campaign['InfluencerCampaign']);
                            $campaign['InfluencerCampaign'] = $tmpicms;
                            $recamp[] = $campaign;
                        }
                    }

                    if ($recamp) {
                        $message = 'Campaigns found';
                        $status = 'SUCCESS';
                        $content = $recamp;
                    } else {
                        $status = 'SUCCESS';
                        $message = 'Campaigns not found';
                        $content = $recamp;
                    }
                    //  $pagination = $this->request->params['paging']['Campaign'];
                    //    unset($pagination['order']);
                    //   unset($pagination['options']);
                    $this->set([
                        // 'pagination' => $pagination,
                        'status' => $status,
                        'message' => $message,
                        'content' => $content,
                        '_serialize' => ['status', 'message', 'content']
                    ]);
                }
            } else {
                $status = 'ERROR';
                $message = 'Invalid Request';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        /*  $this->set([
          'pagination' => $pagination,
          'status' => $status,
          'message' => $message,
          'content' => $content,
          '_serialize' => ['status', 'message', 'content', 'pagination']
          ]);
          $this->render('/' . $this->request->params['ext']); */
    }

    public function api_1_0_changeCampaignStatus() {


        if ($this->request->is('put')) {
            $this->layout = 'json';
            $requesteddata = $this->request->data;
            $this->loadModel('Campaign');
            $isCampaignExist = $this->Campaign->find('first', array('conditions' => array(
                    'Campaign.id' => $requesteddata['campaign_id']
            )));

            if (!empty($isCampaignExist)) {

                $this->loadModel('CampaignStatus');
                $campaignStatus = $this->CampaignStatus->find('first', array('conditions' => array(
                        'CampaignStatus.status_code' => $requesteddata['status_code']
                )));

                $requesteddata['campaign_status_id'] = $campaignStatus['CampaignStatus']['id'];

                $this->Campaign->id = $isCampaignExist['Campaign']['id'];

                $options = array('fields' => array(
                        'User.id',
                        'User.user_role',
                        'Influencer.id'
                    ),
                    'conditions' => array(
                        'User.id' => $requesteddata['user_id']
                    ),
                    'recursive' => 0);
                $this->loadModel('User');
                $role = $this->User->find('first', $options);

                if (!empty($role)) {

                    if ($role['User']['user_role'] == ADMIN) {

                        $this->Campaign->Save($requesteddata);
                    }

                    if ($role['User']['user_role'] == INFLUENCER) {

                        $this->loadModel('InfluencerCampaign');
                        $this->InfluencerCampaign->deleteAll(array('InfluencerCampaign.campaign_id' => $this->Campaign->id,'InfluencerCampaign.influencer_id' => $role['Influencer']['id']), false);
                        $influencerCampaiagnData['influencer_id'] = $role['Influencer']['id'];
                        $influencerCampaiagnData['campaign_id'] = $this->Campaign->id;
                        $influencerCampaiagnData['campaign_status_id'] = $requesteddata['campaign_status_id'];
                        $influencerCampaiagnData['is_read'] = ACTIVE;
                        $sDate = date("Y-m-d H:i:s"); 
                        $influencerCampaiagnData['status_date_time'] = $sDate;
                        $influencerCampaiagnData['read_date_time'] = $sDate;
                        $this->InfluencerCampaign->Save($influencerCampaiagnData);
                    }

                    $status = 'SUCCESS';
                    $message = 'Campaign Status updated.';
                    $content = $requesteddata;
                } else {
                    $status = 'ERROR';
                    $message = 'Invalid User';
                    $content = $requesteddata;
                }
            } else {
                $status = 'Failed';
                $message = 'campaign data not found.';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }
    
    public function api_1_0_influencerCampaignStatus() {

        if ($this->request->is(array('post'))) {

            if (isset($requesteddata['page'])) {
                $page = $requesteddata['page'];
            } else {
                $page = 1;
            }
            if (isset($requesteddata['limit'])) {
                $limit = $requesteddata['limit'];
            } else {
                $limit = 10;
            }
            $requesteddata = $this->request->data;
            $this->loadModel('Influencer');
            $isInfluencerExist = $this->Influencer->find('first', array('conditions' => array(
                    'Influencer.id' => $requesteddata['member_id']
            )));

            if ($isInfluencerExist) {

                $influencer_id = $isInfluencerExist['Influencer']['id'];

                $this->paginate = array(
                    'page' => $page,
                    'limit' => $limit,
                    'fields' => array(
                        'Campaign.id',
                        'Campaign.agency_id',
                        'Campaign.campaign_name',
                        'Campaign.brand_name',
                        'Campaign.hash_tags',
                        'Campaign.content_guidelines',
                        'Campaign.sample_content',
                        'Campaign.last_date_to_apply',
                        'Campaign.start_date',
                        'Campaign.submission_date',
                        'Campaign.is_active',
                        'Campaign.created',
                        'Campaign.modified',
                        'CampaignStatus.id',
                        'InfluencerCampaign.campaign_status_id',
                        'CampaignStatus.status'
                    ),
                    'conditions' => array('InfluencerCampaign.influencer_id' => $influencer_id,
                        'InfluencerCampaign.campaign_status_id' => $requesteddata['status_id']),
                    'recursive' => 1
                );
                $this->loadModel('InfluencerCampaign');
                $campaigns = $this->paginate('InfluencerCampaign');
                $campaigns = Set::extract('/Campaign/.', $campaigns);
                
                if ($campaigns) {
                    $status = 'SUCCESS';
                    $message = 'Campaigns found.';
                    $content = $campaigns;
                } else {
                    $status = 'SUCCESS';
                    $message = 'Campaigns not found.';
                    $content = $campaigns;
                }
                $pagination = $this->request->params['paging']['InfluencerCampaign'];
                    unset($pagination['order']);
                    unset($pagination['options']);
            } else {
                $status = 'Failed';
                $message = 'Influencer not found.';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }

        $this->set([
            'pagination' => $pagination,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content','pagination']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }
    
    public function api_1_0_campaignInfluencerList() {

        if ($this->request->is(array('post'))) {

            if (isset($requesteddata['page'])) {
                $page = $requesteddata['page'];
            } else {
                $page = 1;
            }
            if (isset($requesteddata['limit'])) {
                $limit = $requesteddata['limit'];
            } else {
                $limit = 10;
            }
            $requesteddata = $this->request->data;
            $this->loadModel('Campaign');
            $isCampaignExist = $this->Campaign->find('first', array('conditions' => array(
                    'Campaign.id' => $requesteddata['campaign_id']
            )));
         //   echo "<pre>";print_r($requesteddata);"</pre>";exit;

            if ($isCampaignExist) {

                $campaign_id = $isCampaignExist['Campaign']['id'];

                $this->paginate = array(
                  //  'page' => $page,
                  //  'limit' => $limit,
                    'conditions' => array('InfluencerCampaign.campaign_id' => $campaign_id,
                        'InfluencerCampaign.campaign_status_id' => $requesteddata['status_id']),
                    'recursive' => 3,
                    'order' => array('InfluencerCampaign.id' => 'desc')
                );
               $this->loadModel('Influencer');
                $this->loadModel('InfluencerBankDetail');
                $this->loadModel('SocialProfile');
                $this->loadModel('InfluencerCampaign');
                $this->loadModel('User');
                $this->loadModel('Gender');
                $this->Influencer->unbindModel(
                            array('belongsTo' => array('Campaign','Gender'),'hasMany' => array('InfluencerBankDetail','SocialProfile','InfluencerCampaign','InfluencerCategory','InfluencerLanguage'))
                        );
                $this->InfluencerCampaign->unbindModel(
                            array('belongsTo' => array('Campaign','CampaignStatus'))
                        );
                $this->User->unbindModel(
                            array('hasOne' => array('Agency','Admin','Influencer'))
                        );
                $this->Gender->unbindModel(
                            array('hasMany' => array('CampaignFilter','CampaignGender'))
                        );
                $influencers = $this->paginate('InfluencerCampaign');
                $recamp = [];
            if (!empty($influencers)) {
                foreach ($influencers as $influencer) {
                    $tmpicms = [];

		    $influencer['Influencer']['followers'] = $influencer['Influencer']['User']['SocialProfile']['followers'];
                   $influencer['Influencer']['user_name'] = $influencer['Influencer']['User']['SocialProfile']['user_name'];
                   
                   unset($influencer['Influencer']['User']);
                    $recamp [] = $influencer;
                    
                }
           }
            $result12 = Set::classicExtract($recamp, '{n}.Influencer');
              // echo"<pre>";print_r($campaigns);"</pre>";exit;

                if ($result12) {
                    $status = 'SUCCESS';
                    $message = 'Influencers found.';
                    $content = $result12;
                } else {
                    $status = 'SUCCESS';
                    $message = 'Influencers not found.';
                    $content = $influencers;
                }
               // $pagination = $this->request->params['paging']['InfluencerCampaign'];
                 //   unset($pagination['order']);
                //    unset($pagination['options']);
            } else {
                $status = 'Failed';
                $message = 'Campaign not found.';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }

        $this->set([
         //   'pagination' => $pagination,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }
    
    
    public function api_1_0_campaignIgnoredInfluencerList() {


        if ($this->request->is(array('post'))) {

            if (isset($requesteddata['page'])) {
                $page = $requesteddata['page'];
            } else {
                $page = 1;
            }
            if (isset($requesteddata['limit'])) {
                $limit = $requesteddata['limit'];
            } else {
                $limit = 10;
            }
            $requesteddata = $this->request->data;
            $this->loadModel('Campaign');
            $isCampaignExist = $this->Campaign->find('first', array('conditions' => array(
                    'Campaign.id' => $requesteddata['campaign_id']
            )));

            if ($isCampaignExist) {

                $campaign_id = $isCampaignExist['Campaign']['id'];

                $this->loadModel('InfluencerCampaign');
                $influencer_ids = $this->InfluencerCampaign->find('all', array('fields' => array('influencer_id'),
                    'conditions' => array('InfluencerCampaign.campaign_id' => $campaign_id,
                        'InfluencerCampaign.campaign_status_id' == 4 and
                        'InfluencerCampaign.campaign_status_id' == 5)));


                $Inids = Set::classicExtract($influencer_ids, '{n}.InfluencerCampaign.influencer_id');

                $this->loadModel('Influencer');
                $this->loadModel('InfluencerBankDetail');
                $this->loadModel('InfluencerCampaign');
                $this->loadModel('SocialProfile');
                $this->loadModel('Admin');
                $this->loadModel('Agency');
                $this->loadModel('CampaignGender');
                $this->loadModel('Gender');
                $this->loadModel('InfluencerCategory');
                $this->loadModel('CampaignFilter');
                $this->loadModel('InfluencerLanguage');
                $this->loadModel('User');
                $this->Influencer->unbindModel(
                        array('hasMany' => array('InfluencerBankDetail', 'InfluencerCampaign', 'SocialProfile'))
                );
                $this->User->unbindModel(
                        array('hasOne' => array('Influencer', 'Admin', 'Agency'))
                );
                $this->Gender->unbindModel(
                        array('hasMany' => array('CampaignGender', 'CampaignFilter'))
                );
                $this->InfluencerCategory->unbindModel(
                        array('belongsTo' => array('Influencer'))
                );
                $this->InfluencerLanguage->unbindModel(
                        array('belongsTo' => array('Influencer'))
                );



                $this->paginate = array(
                    //'page' => $page,
                    'limit' => 1000,
                    'conditions' => array(
                        'Influencer.id !=' => $Inids
                    ),
                    'recursive' => 3,
                    'order' => array('Influencer.id' => 'desc'));


                $influencers = $this->paginate('Influencer');
                $recamp = [];

                if (!empty($influencers)) {
                    foreach ($influencers as $influencer) {
                        $tmpicms = [];

                        $influencer['Influencer']['followers'] = $influencer['User']['SocialProfile']['followers'];
                       $influencer['Influencer']['user_name'] = $influencer['User']['SocialProfile']['user_name'];
                        $recamp [] = $influencer;
                    }
                }
                $result12 = Set::classicExtract($recamp, '{n}.Influencer');
                $this->log($result12,'debug');
                if ($result12) {
                    $status = 'SUCCESS';
                    $message = 'Influencers found.';
                    $content = $result12;
                } else {
                    $status = 'SUCCESS';
                    $message = 'Influencers not found.';
                    $content = $influencers;
                }
             
            } else {
                $status = 'Failed';
                $message = 'Campaign not found.';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }

        $this->set([
       
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

        public function api_1_0_getCampaignsAdmin() {
        $content = NULL;
        $pagination = null;
        if ($this->request->is('post')) {

            $requesteddata = $this->request->data;


            $conditions = [];
            if (isset($requesteddata['status_id'])) {
                $campaignStatus = $requesteddata['status_id'];
                $conditions = array('Campaign.is_active' => ACTIVE, 'Campaign.campaign_status_id' => $campaignStatus);
            } else {
                $conditions = array('Campaign.is_active' => ACTIVE);
            }

            $fields = array(
                'Campaign.id',
                'Campaign.agency_id',
                'Campaign.campaign_name',
                'Campaign.brand_name',
                'Campaign.product_name',
                'Campaign.hash_tags',
                'Campaign.total_budget',
                'Campaign.content_guidelines',
                'Campaign.sample_content',
                'Campaign.last_date_to_apply',
                'Campaign.start_date',
                'Campaign.submission_date',
                'Campaign.is_active',
                'Campaign.created'
            );
            $this->paginate = array(
                //'page' => $page,
                'limit' => 100,
                'fields' => $fields,
                'conditions' => $conditions,
                'order' => array(
                    'Campaign.id' => 'desc'
                ),
                'recursive' => 3
            );

            $this->loadModel('Campaign');
            $this->loadModel('CampaignDeliverable');
            $this->loadModel('CampaignStatus');
            $this->loadModel('CampaignGoal');
            $this->loadModel('CampaignGender');
            $this->loadModel('CampaignAgeGroup');
            $this->loadModel('CampaignInfluencerType');
            $this->loadModel('CampaignCategory');
            $this->loadModel('CampaignLocation');
            $this->loadModel('CampaignRule');
            $this->loadModel('CampaignReward');
            $this->loadModel('Deliverable');
            $this->loadModel('SocialNetwork');
            $this->loadModel('Goal');
            $this->loadModel('CampaignGoal');
            $this->loadModel('CampaignFilter');
            $this->loadModel('Gender');
            $this->loadModel('AgeGroup');
            $this->loadModel('InfluencerType');
            $this->loadModel('Location');

            $this->Campaign->unbindModel(
                    array('belongsTo' => array('Agency', 'CampaignStatus'), 'hasMany' => array('Notification', 'CampaignDoc', 'InfluencerCampaign', 'CampaignDeliverable'))
            );
            $this->CampaignStatus->unbindModel(
                    array('hasMany' => array('Campaign', 'InfluencerCampaign'))
            );
            $this->CampaignGoal->unbindModel(
                    array('belongsTo' => array('Campaign'))
            );
            $this->CampaignGender->unbindModel(
                    array('belongsTo' => array('Campaign'))
            );
            $this->CampaignAgeGroup->unbindModel(
                    array('belongsTo' => array('Campaign'))
            );
            $this->CampaignInfluencerType->unbindModel(
                    array('belongsTo' => array('Campaign'))
            );
            $this->CampaignCategory->unbindModel(
                    array('belongsTo' => array('Campaign'))
            );
            $this->CampaignLocation->unbindModel(
                    array('belongsTo' => array('Campaign'))
            );
            $this->CampaignRule->unbindModel(
                    array('belongsTo' => array('Campaign'))
            );
            $this->CampaignReward->unbindModel(
                    array('belongsTo' => array('Campaign'))
            );
            $this->CampaignDeliverable->unbindModel(
                    array('belongsTo' => array('Campaign'))
            );
            $this->Goal->unbindModel(
                    array('hasMany' => array('CampaignFilter', 'CampaignGoal'))
            );
            $this->Gender->unbindModel(
                    array('hasMany' => array('CampaignFilter', 'CampaignGender'))
            );
            $this->AgeGroup->unbindModel(
                    array('hasMany' => array('CampaignFilter', 'CampaignAgeGroup'))
            );
            $this->InfluencerType->unbindModel(
                    array('hasMany' => array('CampaignFilter', 'CampaignInfluencerType'))
            );
            $this->Location->unbindModel(
                    array('hasMany' => array('CampaignLocation'))
            );
            $this->Deliverable->unbindModel(
                    array('hasMany' => array('CampaignDeliverable'), 'belongsTo' => array('Campaign'))
            );

            $campaigns = $this->paginate('Campaign');

            $recamp = [];
            if (!empty($campaigns)) {
                foreach ($campaigns as $campaign) {
                    $tmpicms = [];


                    $goal = Set::classicExtract($campaign['CampaignGoal'], '{n}.Goal.goal');
                    $gender = Set::classicExtract($campaign['CampaignGender'], '{n}.Gender.gender');
                    $category = Set::classicExtract($campaign['CampaignCategory'], '{n}.Category.category');
                    $influencerType = Set::classicExtract($campaign['CampaignInfluencerType'], '{n}.InfluencerType.influencer_type');
                    $location = Set::classicExtract($campaign['CampaignLocation'], '{n}.Location.location');
                    $reward_amount = Set::classicExtract($campaign['CampaignReward'], '{n}.amount');
                    //$reward_amount=$campaign['CampaignReward']['amount'];
                    $reward_type = Set::classicExtract($campaign['CampaignReward'], '{n}.reward_type');
                    $age_group = Set::classicExtract($campaign['CampaignAgeGroup'], '{n}.AgeGroup.age_group');


                    $goal2 = implode(",", $goal);
                    $gender2 = implode(",", $gender);
                    $category2 = implode(",", $category);
                    $influencerType2 = implode(",", $influencerType);
                    $location2 = implode(",", $location);
                    $age_group2 = implode(",", $age_group);
                    $reward_amount2 = implode(",", $reward_amount);
                    $reward_type2 = implode(",", $reward_type);

                    $campaign['Campaign']['goal'] = $goal2;
                    $campaign['Campaign']['gender'] = $gender2;
                    $campaign['Campaign']['category'] = $category2;
                    $campaign['Campaign']['influencer_type'] = $influencerType2;
                    $campaign['Campaign']['location'] = $location2;
                    $campaign['Campaign']['reward_amount'] = $reward_amount2;
                    $campaign['Campaign']['reward_type'] = $reward_type2;
                    $campaign['Campaign']['age_group'] = $age_group2;

                    $recamp [] = $campaign;
                }
            }
            $result12 = Set::classicExtract($recamp, '{n}.Campaign');
            // echo"<pre>";print_r($campaigns);"</pre>";exit;
            //$campaigns = Set::extract('/Campaign/.', $campaigns);
            //unset($campaigns['CampaignStatus']);
            if ($result12) {
                $message = 'Campaigns found';
                $status = 'SUCCESS';
                $content = $result12;
            } else {
                $status = 'SUCCESS';
                $message = 'Campaigns not found';
                $content = $result12;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }

        $this->set([

            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

}
