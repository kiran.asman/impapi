<?php

App::uses('ApiController', 'Api.Controller');


class GoalsController extends ApiController {

    /**
     * Components
     *
     * @var array
     * */
    public $components = array('Paginator', 'RequestHandler',);
    
    public function beforeFilter() {
        parent::beforeFilter();
        
    }
    
    /**
     * @method getGenders
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/goals/getGoals/
     * REQUEST :  NULL
     * METHOD : GET or POST    
     * RESPONSE SUCCESS :
     * {
            "status": "SUCCESS",
            "message": "Age groups found",
            "content": [
                {
                    "id": "14",
                    "goal": "App installations"
                },
                {
                    "id": "13",
                    "goal": "Website clicks"
                }
     *          ...
     *          ...
     * 
            ],
            "pagination": {
                "page": 1,
                "current": 10,
                "count": 14,
                "prevPage": false,
                "nextPage": true,
                "pageCount": 2,
                "limit": 10,
                "paramType": "named"
            }
        }
     * 
     */
    
    public function api_1_0_getGoals() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;                
        }
        if ($this->request->is('get')) {
            $requesteddata = $this->request->params['named'];
        }
        
        if(isset($requesteddata['page'])){
            $page = $requesteddata['page'];
        }else{
           $page = 1; 
        }
        if(isset($requesteddata['limit'])){
            $limit = $requesteddata['limit'];
        }else{
            $limit = 10;
        }
            
        $this->paginate = array(
            'page' => $page,
            'limit' => $limit, 
              'fields' => array(
              'id',
              'goal',
                  ), 
            'conditions' => array(
                'Goal.is_active' => ACTIVE,
                ),
            'recursive' => -1,
            'order' => array('Goal.id' => 'desc')
        );
        $this->loadModel('Goal');
        $goals = $this->paginate();
        $goals = Set::extract('/Goal/.', $goals);
        if ($goals) {
            $message = 'Goals found';
            $status = 'SUCCESS';
            $content = $goals;
        } else {
            $status = 'SUCCESS';
            $message = 'Goals not found';
            $content = $goals;
        }
        $pagination = $this->request->params['paging']['Goal'];
        unset($pagination['order']);
        unset($pagination['options']);
        $this->set([
            'pagination' => $pagination,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content', 'pagination']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }
    
  
}
