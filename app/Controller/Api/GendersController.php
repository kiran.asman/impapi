<?php

App::uses('ApiController', 'Api.Controller');


class GendersController extends ApiController {

    /**
     * Components
     *
     * @var array
     * */
    public $components = array('Paginator', 'RequestHandler',);
    
    public function beforeFilter() {
        parent::beforeFilter();
        
    }
    
    /**
     * @method getGenders
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/genders/getGenders/
     * REQUEST :  NULL
     * METHOD : GET or POST    
     * RESPONSE SUCCESS :
     * {
            "status": "SUCCESS",
            "message": "Age groups found",
            "content": [
                {
                    "id": "14",
                    "age_group": "All"
                },
                {
                    "id": "13",
                    "age_group": "35 - 44"
                }
     *          ...
     *          ...
     * 
            ],
            "pagination": {
                "page": 1,
                "current": 10,
                "count": 14,
                "prevPage": false,
                "nextPage": true,
                "pageCount": 2,
                "limit": 10,
                "paramType": "named"
            }
        }
     * 
     */
    
    public function api_1_0_getGenders() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;                
        }
        if ($this->request->is('get')) {
            $requesteddata = $this->request->params['named'];
        }
        
        if(isset($requesteddata['page'])){
            $page = $requesteddata['page'];
        }else{
           $page = 1; 
        }
        if(isset($requesteddata['limit'])){
            $limit = $requesteddata['limit'];
        }else{
            $limit = 10;
        }
            
        $this->paginate = array(
            'page' => $page,
            'limit' => $limit, 
              'fields' => array(
              'id',
              'gender',
                  ), 
            'conditions' => array(
                'Gender.is_active' => ACTIVE,
                ),
            'recursive' => -1,
            'order' => array('Gender.id' => 'desc')
        );
        $this->loadModel('Gender');
        $genders = $this->paginate();
        $genders = Set::extract('/Gender/.', $genders);
        if ($genders) {
            $message = 'Genders found';
            $status = 'SUCCESS';
            $content = $genders;
        } else {
            $status = 'SUCCESS';
            $message = 'Genders not found';
            $content = $genders;
        }
        $pagination = $this->request->params['paging']['Gender'];
        unset($pagination['order']);
        unset($pagination['options']);
        $this->set([
            'pagination' => $pagination,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content', 'pagination']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }
    
    
}
