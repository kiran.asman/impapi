<?php
App::uses('AppModel', 'Model');
/**
 * SocialNetwork Model
 *
 * @property SocialProfile $SocialProfile
 */
class SocialNetwork extends AppModel {

    
    public $actsAs = array('Sluggable.Sluggable' => array(
            'label' => 'social_network_name',
            'slug' => 'social_network_code',
            'lowercase' => false, // Do we lowercase the slug ?
            'uppercase' => true, // Do we uppercase the slug ?
            'separator' => '-',
            'length' => 250,
            'overwrite' => true
    ));
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'social_network_name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
                        'unique' => array(
                        'rule' => array('isUniqueNetwork'),
                        'message' => 'This social network is already exist.'
            ),
		),
		'social_network_code' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'social_network_secret_key' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'social_networkid' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_active' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'SocialProfile' => array(
			'className' => 'SocialProfile',
			'foreignKey' => 'social_network_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Deliverable' => array(
			'className' => 'Deliverable',
			'foreignKey' => 'social_network_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
/**
     * Before isUniqueNetwork
     * @param array $options
     * @return boolean
     */
    function isUniqueNetwork($check) {

        $network = $this->find(
                'first', array(
            'fields' => array(
                'SocialNetwork.id',
                'SocialNetwork.social_network_name'
            ),
            'conditions' => array(
                'SocialNetwork.social_network_name' => $check['social_network_name']
            )
                )
        );

        if (!empty($network)) {
            if ($this->data[$this->alias]['social_network_name'] != $network['SocialNetwork']['social_network_name']) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
}
