<?php

App::uses('AppModel', 'Model');

/**
 * DocType Model
 *
 * @property Campaign $Campaign
 */
class CampaignInfluencerType extends AppModel {

   
    public $belongsTo = array(
        'Campaign' => array(
            'className' => 'Campaign',
            'foreignKey' => 'campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'InfluencerType' => array(
            'className' => 'InfluencerType',
            'foreignKey' => 'influencer_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
