<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * CakePHP Language
 * @author srikanth
 */
class Language extends AppModel {
    
    public $hasMany = array(
		'InfluencerLanguage' => array(
			'className' => 'InfluencerLanguage',
			'foreignKey' => 'influencer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
    );
}
