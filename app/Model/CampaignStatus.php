<?php
App::uses('AppModel', 'Model');
/**
 * CampaignStatus Model
 *
 * @property Campaign $Campaign
 * @property InfluencerCampaign $InfluencerCampaign
 */
class CampaignStatus extends AppModel {

    
    public $actsAs = array('Sluggable.Sluggable' => array(
            'label' => 'status',
            'slug' => 'status_code',
            'lowercase' => false, // Do we lowercase the slug ?
            'uppercase' => true, // Do we uppercase the slug ?
            'separator' => '-',
            'length' => 350,
            'overwrite' => true
    ));
    
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'status' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Status can\'t be blank',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
                        'unique' => array(
                            'rule' => array('isUniqueStatus'),
                            'message' => 'This category is already exist.'
                        ),
		),
		'status_code' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Status code can\'t be blank',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Campaign' => array(
			'className' => 'Campaign',
			'foreignKey' => 'campaign_status_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InfluencerCampaign' => array(
			'className' => 'InfluencerCampaign',
			'foreignKey' => 'campaign_status_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

        /**
     * Before isUniqueStatus
     * @param array $options
     * @return boolean
     */
    function isUniqueStatus($check) {

        $status = $this->find(
                'first', array(
            'fields' => array(
                'CampaignStatus.id',
                'CampaignStatus.status'
            ),
            'conditions' => array(
                'CampaignStatus.status' => $check['status']
            )
                )
        );

        if (!empty($status)) {
            if ($this->data[$this->alias]['status'] != $status['CampaignStatus']['status']) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
}
