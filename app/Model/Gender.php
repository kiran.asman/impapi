<?php

App::uses('AppModel', 'Model');

/**
 * Gender Model
 *
 * @property CampaignFilter $CampaignFilter
 */
class Gender extends AppModel {

  
    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'CampaignFilter' => array(
            'className' => 'CampaignFilter',
            'foreignKey' => 'genders',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'CampaignGender' => array(
            'className' => 'CampaignGender',
            'foreignKey' => 'gender_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
