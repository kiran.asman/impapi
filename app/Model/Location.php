<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * CakePHP Location
 * @author srikanth
 */
class Location extends AppModel {
    
     public $hasMany = array(
        
        'CampaignLocation' => array(
            'className' => 'CampaignLocation',
            'foreignKey' => 'location_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    
}
