<?php

App::uses('AppModel', 'Model');

/**
 * CampaignRule Model
 *
 * @property Goal $Goal
 */
class CampaignRule extends AppModel {

  
    public $belongsTo = array(
        'Campaign' => array(
            'className' => 'Campaign',
            'foreignKey' => 'campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
