<?php

App::uses('AppModel', 'Model');

/**
 * DocType Model
 *
 * @property Campaign $Campaign
 */
class Deliverable extends AppModel {

    
    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'CampaignDeliverable' => array(
            'className' => 'CampaignDeliverable',
            'foreignKey' => 'deliverable_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    public $belongsTo = array(
        'Campaign' => array(
            'className' => 'Campaign',
            'foreignKey' => 'campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'SocialNetwork' => array(
            'className' => 'SocialNetwork',
            'foreignKey' => 'social_network_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
