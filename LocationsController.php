<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP LocationsController
 * @author srikanth
 */
class LocationsController extends AppController {
    
     /**
     * Components
     *
     * @var array
     * */
    public $components = array('Paginator', 'RequestHandler',);
    
    public function beforeFilter() {
        parent::beforeFilter();
        
    }
    
    
       /**
     * @method getCategories
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/locations/getLocations/
     * REQUEST :  NULL
     * METHOD : GET or POST    
     * RESPONSE SUCCESS :
     * {
            "status": "SUCCESS",
            "message": "Locations found",
            "content": [
                {
                    "id": "14",
                    "category": "General"
                },
                {
                    "id": "13",
                    "category": "Games"
                }
     *          ...
     *          ...
     * 
            ],
            "pagination": {
                "page": 1,
                "current": 10,
                "count": 14,
                "prevPage": false,
                "nextPage": true,
                "pageCount": 2,
                "limit": 10,
                "paramType": "named"
            }
        }
     * 
     */
    
    public function api_1_0_getLocations() {
        
        if ($this->request->is('post')) {
                $requesteddata = $this->request->data;
            }
        if ($this->request->is('get')) {
                $requesteddata = $this->request->params['named'];
            }
        
        if(isset($requesteddata['page'])){
            $page = $requesteddata['page'];
        }else{
           $page = 1; 
        }
        if(isset($requesteddata['limit'])){
            $limit = $requesteddata['limit'];
        }else{
            $limit = 10;
        }
        
        $this->paginate = array(
            'page' => $page,
            'limit' => $limit, 
              'fields' => array(
              'id',
              'location',
             // 'description'
                  ), 
            'conditions' => array(
                'Location.is_active' => ACTIVE,
                ),
            'recursive' => -1,
            'order' => array('Location.id' => 'desc')
        );
        $this->loadModel('Location');
        $locations = $this->paginate();
        $locations = Set::extract('/Location/.', $locations);
        if ($locations) {
            $message = 'Locations found';
            $status = 'SUCCESS';
            $content = $locations;
        } else {
            $status = 'SUCCESS';
            $message = 'Locations not found';
            $content = $locations;
        }
        $pagination = $this->request->params['paging']['Location'];
        unset($pagination['order']);
        unset($pagination['options']);
        //$this->set(compact('services',$services));
        $this->set([
            'pagination' => $pagination,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content', 'pagination']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

 

}
